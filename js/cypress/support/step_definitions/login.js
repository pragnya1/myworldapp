var emailValue;

var passwordValue;

var error;

given("Open Application and Enter url", () => {

  cy.visit('http://sprinkle-burn.glitch.me/')

    
});


given("the user has the correct credentials", () => {

  emailValue = "test@drugdev.com";

  passwordValue = "supers3cret";

});



given("the user has the incorrect credentials", () => {

  emailValue = "incorrect@drugdev.com";

  passwordValue = "sers3cret";

});



when("the user enters username", () => {

  cy.get('input[type=email]').type(emailValue).should('have.value', emailValue)
  
});



then("the user enters password", () => {

  cy.get('input[type=password]').type(passwordValue).should('have.value', passwordValue)
  
});



then("clicks Login", () => {

  cy.get('.bg-dark-green').click()
	
	});



then("the user is presented with a welcome message", () => {

  if ( cy.contains('Welcome Dr I Test'))
 {
  cy.log("login Successful")
 }

});



then("the user is presented with a error message", () => {

  if(cy.contains('Credentials are incorrect'))
    {
      
      cy.log("login not Successful")
    }
  
});